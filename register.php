<?php
session_start();

include('includes/config.php');
include('includes/db.php');

if(isset($_POST['register'])){
  $_SESSION['name'] = $_POST['name'];
  $_SESSION['email'] = $_POST['email'];
  $_SESSION['password'] = $_POST['password'];
  $_SESSION['confirm_password'] = $_POST['confirm_password'];

  
  // Validation
  if (strlen($_POST['name']) < 3) {
    header("Location: register.php?err=" . urlencode("The name must be at least 3 characters long"));
    exit();
  } else if ($_POST['password'] != $_POST['confirm_password']) {
    header("Location: register.php?err=" . urlencode("Confirm password do not match"));
    exit();
  } else if (strlen($_POST['password']) < 5) {
    header("Location: register.php?err=" . urlencode("The password should be at least 5 characters"));
    exit();
  } else if (strlen($_POST['confirm_password']) < 5) {
    header("Location: register.php?err=" . urlencode("The confirm password should be at least 5 characters"));
    exit();
  }
}

?>


<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Register</title>

    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.css" rel="stylesheet">

    <!-- Custom styles for this template -->
    <link href="css/style.css" rel="stylesheet">
  </head>

  <body>

    <nav class="navbar navbar-inverse navbar-fixed-top">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#">Project name</a>
        </div>
        <div id="navbar" class="collapse navbar-collapse">
          <ul class="nav navbar-nav">
            <li><a href="index.php">Login</a></li>
            <li class="active"><a href="register.php">Register</a></li>
          </ul>
        </div><!--/.nav-collapse -->
      </div>
    </nav>

    <div class="container">
      <div class="row">
        <div class="col-xs-12">    
          <form method="post" action="register.php">
          <h2>Register Here</h2>
          <?php if (isset($_GET['err'])) { ?>
          <div class="alert alert-danger">
            <?php 
              echo $_GET['err'];
            ?>
          </div>
          <?php } ?>
          <hr>
            <div class="form-group">
              <label>Name</label>
              <input type="text" name="name" class="form-control" placeholder="Name" required value="<?php echo @$_SESSION['name']; ?>">
            </div>
      		  <div class="form-group">
      		    <label for="exampleInputEmail1">Email address</label>
      		    <input type="email" name="email" class="form-control" placeholder="Email" required value="<?php echo @$_SESSION['email']; ?>">
      		  </div>
      		  <div class="form-group">
      		    <label for="exampleInputPassword1">Password</label>
      		    <input type="password" name="password" class="form-control" placeholder="Password" required value="<?php echo @$_SESSION['password']; ?>">
      		  </div>
            <div class="form-group">
              <label>Confirm Password</label>
              <input type="password" name="confirm_password" class="form-control" placeholder="Confirm Password" required value="<?php echo @$_SESSION['confirm_password']; ?>">
            </div>  		  
      		  <button type="submit" class="btn btn-default" name="register">Register</button>
    		  </form>
        </div>
      </div>
    </div><!-- /.container -->


    <!-- Bootstrap core JavaScript
    ================================================== -->
    <!-- Placed at the end of the document so the pages load faster -->
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
    <script src="js/bootstrap.js"></script>
  </body>
</html>
